import com.sun.org.apache.bcel.internal.generic.BIPUSH;

import java.math.BigInteger;

public class WhiteRabbit {
    private int numberOfStages;
    private int maxAmountOfStages;
    private BigInteger[] matrix;


    WhiteRabbit(int numberOfStages, int maxAmountOfStages) {
        this.numberOfStages = numberOfStages;
        this.maxAmountOfStages = maxAmountOfStages;
        matrix = new BigInteger[numberOfStages + 1];
        matrix[0] = BigInteger.ONE;
    }


    public BigInteger getNumberOfWays() {
        for (int i = 1; i <= numberOfStages; i++) {
            int startStage = Math.max(0, i - maxAmountOfStages);
            matrix[i] = BigInteger.ZERO;
            for (int j = startStage; j < i; j++) {
                matrix[i] = matrix[i].add(matrix[j]);
            }
        }

        return matrix[numberOfStages];
    }
}
