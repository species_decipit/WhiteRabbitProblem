import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(new FileReader("input.txt"));
        PrintWriter wr = new PrintWriter("output.txt");

        int N, K;
        WhiteRabbit myWhiteRabbit;

        K = sc.nextInt();
        N = sc.nextInt();

        myWhiteRabbit = new WhiteRabbit(N, K);

        wr.print(myWhiteRabbit.getNumberOfWays());

        sc.close();
        wr.close();
    }
}
